import pandas as pd
import joblib
import numpy as np

from flask_wtf import FlaskForm
from wtforms import StringField, FileField
from wtforms.validators import DataRequired

from flask import Flask, flash, request, redirect, url_for, jsonify, abort, render_template, send_file
from werkzeug.utils import secure_filename
import os


app = Flask(__name__)

knn_iris = joblib.load('knn_iris.pkl')


@app.route('/')
def index():
    return render_template('templates/index.html')


# host:5000/iris/ -> return score
# iris dataset has only 3 classes
@app.route('/iris_form/', methods=('GET', 'POST'))
def iris_form():
    if request.method == 'POST':
        lst = []
        for i in range(1, 5):
            lst.append(request.form[f'title{i}'])
        predict = knn_iris.predict(np.array(lst, dtype=float).reshape(1, -1))
        if predict == 0:
            return '<img src="/static/setosa.jpg" alt="setosa">'
        elif predict == 1:
            return '<img src="/static/versicolor.jpg" alt="versicolor">'
        elif predict == 2:
            return '<img src="/static/verginica.jpg" alt="verginica">'
    return render_template('templates/iris_form.html')


# api: script can receive POST request, for ex, from curl:
# curl --header "Content-Type: application/json"\
#     --request POST\
#     --data '{"flower":"1,241,3,7"}'\
#     http://localhost:5000/iris_post
@app.route('/iris_post', methods=['POST'])
def add_message():
    try:
        content = request.get_json()
        param = content['flower'].split(',')
        param = [float(num) for num in param]
        param = np.array(param).reshape(1, -1)
        predict = knn_iris.predict(param)
        predict = {'class': str(predict[0])}
    except:
        return redirect(url_for('bad_request'))
    return jsonify(predict)


# security
app.config.update(dict(
    SECRET_KEY="powerful secretkey",
    WTF_CSRF_SECRET_KEY="a csrf secret key"
))


class MyForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    file = FileField()


# user can send csv file, csv with model scoring will be sent in response
@app.route('/iris_csv/', methods=['GET', 'POST'])
def iris_csv():
    form = MyForm()
    if form.validate_on_submit():
        filename = form.name.data + '.csv'

        df = pd.read_csv(form.name.data, header=None)
        predict = knn_iris.predict(df)
        result = pd.DataFrame(predict)
        result.to_csv(filename, index=False)

        return send_file(filename,
                         mimetype='text/csv',
                         attachment_filename=filename,
                         as_attachment=True)

    return render_template('templates/iris_csv.html', form=form)


UPLOAD_FOLDER = ''
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# not only users, but services can do the same
@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename + 'uploaded')
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return 'file uploaded'

    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
